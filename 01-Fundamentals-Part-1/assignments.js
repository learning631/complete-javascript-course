const country = 'Ukraine';
const continent = 'Europe';
let population = '44.13 m';
const isIsland = false;
const language = 'ukraine';

console.log('Values and Variables');
console.log(country);
console.log(continent);
console.log(population);
console.log('');
console.log('Data Types');
console.log('isIsland - ', typeof isIsland);
console.log('country - ', typeof country);
console.log('language - ', typeof language);
console.log('population - ', typeof population);
console.log('');
console.log('let, const and var');
